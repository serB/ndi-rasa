## happy path
* greet
  - utter_greet
* mood_great
  - utter_happy

## sad path 1
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* affirm
  - utter_happy

## sad path 2
* greet
  - utter_greet
* mood_unhappy
  - utter_cheer_up
  - utter_did_that_help
* deny
  - utter_goodbye

## bot challenge
* bot_challenge
  - utter_iamabot

## New Story

* greet
    - utter_greet
* mood_unhappy
    - utter_cheer_up
    - utter_did_that_help
* affirm
    - utter_happy

## New Story

* greet
    - utter_greet
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_greet
* mood_unhappy
    - utter_cheer_up

## New Story

* greet
    - utter_greet
* mood_great
    - utter_happy

## New Story

* who_are_you
    - utter_whoami

## New Story

* bot_challenge
    - utter_iamabot
* mood_great
    - utter_happy

## New Story

* greet
    - utter_greet
* who_are_you
    - utter_whoami

## New Story

* greet
    - utter_greet
    - utter_offer_help

## New Story

* greet
    - utter_greet
    - utter_offer_help
* who_are_you
    - utter_whoami
* bot_challenge
    - utter_iamabot

## New Story

* who_are_you
    - utter_whoami
* mood_great
    - utter_happy
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_greet
* who_are_you
    - utter_whoami
* bot_challenge
    - utter_iamabot

## New Story

* what_is_this_app
    - utter_explain_app

## New Story

* greet
    - utter_greet
    - utter_offer_help
* what_is_this_app
    - utter_explain_app
* who_are_you
    - utter_whoami
* affirm
    - utter_did_that_help
* affirm
    - utter_happy

## New Story

* greet
    - utter_greet
    - utter_offer_help
* what_is_this_app
    - utter_explain_app
* mood_great
    - utter_happy
* goodbye
    - utter_goodbye

## New Story

* greet
    - utter_greet
* ask_for_help
    - utter_offer_help
* what_is_this_app
    - utter_explain_app

## New Story

* who_are_you
    - utter_whoami
* what_is_this_app
    - utter_explain_app
* mood_great
    - utter_happy

## New Story

* greet
    - utter_greet
    - utter_offer_help
* deny
    - utter_goodbye

## New Story

* who_are_you
    - utter_what_is_my_purpose

## New Story

* greet
    - utter_greet
* ask_for_joke
    - utter_joke
* thanks
    - utter_your_welcome

## New Story

* ask_for_joke
    - utter_joke

## New Story

* who_are_you
    - utter_whoami
* what_is_this_bot
    - utter_what_is_my_purpose

## New Story

* what_is_this_bot
    - utter_what_is_my_purpose
