## intent:greet
- salut
- salut !
- hey
- bonjour
- bonjour !
- hey !
- yo
- yo !
- wesh
- plop
- wesh !
- bien ou bien
- ça va ?

## intent:goodbye
- Au revoir !
- A plus
- À plus
- À plus tard
- À la prochaine
- bye
- Bye
- bye !

## intent:affirm
- oui
- ouais
- en effet
- bien évidemment
- bien sur
- bien-sûr
- évidemment
- bien-sur
- correct
- correct !
- D'accord

## intent:deny
- non
- jamais
- jamais de la vie
- surtout pas
- pas vraiment
- il y a pas moyen
- pas du tout

## intent:mood_great
- parfait
- très bien
- super
- merveilleux
- extraordinaire
- je me sens très bien
- je suis super
- je vais bien
- Cool

## intent:mood_unhappy
- triste
- très triste
- mécontent
- mauvais
- très mauvais
- terrible
- horrible
- pas très bien
- tellement triste
- Je vais mal

## intent:bot_challenge
- es-tu un bot ?
- es-tu un humain ?
- est-ce que je parle à un bot ?
- est-ce que je parle à un humain ?
- t'es un bot ?
- t'es un humain ?
- tu es un bot ?
- tu es un humain ?

## intent:who_are_you
- Qui es-tu ?
- J'aimerais savoir comment tu t'appelles
- J'aimerais connaitre ton nom
- Tu peux me dire ton prénom ?
- T'es qui ?
- Comment tu t'appelle ?

## intent:what_is_this_app
- Il sert à quoi ce site ?
- C'est quoi cette appli ?
- Qu'est ce que c'est que ce site ?
- Tu peux m'expliquer ce qu'est ce site ?
- Tu peux me dire ce que c'est ce site.

## intent:what_is_this_bot
- Tu sers à quoi ?
- Tu peux faire quoi ?
- Quelles sont tes fonctionnalités
- Qu'est ce que tu peux faire ?
- Quelles sont tes compétences ?

## intent:ask_for_help
- Tu peux m'aider ?
- J'ai besoin d'aide
- Aide moi s'il te plait !
- Tu peux me rendre service ?
- J'aimerais que tu m'aides

## intent:thanks
- Merci beaucoup !
- Merci Didier !
- merci
- thanks
- Je te remercie

## intent:ask_for_joke
- Raconte moi une blague
- tu peux me raconter une blague ?
- fais moi rire
- dis moi quelque chose de drôle

## intent:vulgarite
- connard
- enculé
- batard
- fils de pute
- zoreille
- con
- t'es un con
- t'es débile
- débile
