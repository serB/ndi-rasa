# Didier : Chatbot en Rasa
## Rasa
Rasa est un framework open-source composé de deux librairies. Rasa NLU (Natural Language Understanding) qui permet de l'intention d'une phrase en langage naturel. Et Rasa Core, qui permet de créer un fil de discussion cohérent et de choisir la réponse la plus adaptée.
## Démonstation du bot
En vous rendant sur notre site à l'adresse XXXX, une version fonctionnelle de Didier est accessible. Il suffit de cliquer sur la bulle en bas à droite de l'écran pour ouvrir le chat.
## Easter-egg
Pour découvrir un de nos easter-egg, essayez de demander à Didier ce qu'il sait faire, ou à quoi il sert (il faudra surement essayer quelques fois étant donné qu'il peut répondre plusieurs phrases différentes ;) )
